import { useState } from 'react';
import './App.css';
import { ReactGrid } from "@silevis/reactgrid";
import "@silevis/reactgrid/styles.css";

const MIN_YEAR=2023, MAX_YEAR=2050;

function App() {
    const [dataPV, setDataPV] = useState({
        "puissance-pv-sol": {},
    });
    const [dataBois, setDataBois] = useState({
        "production-bois": {},
    })
    const [dataSolaireCombine, setDataSolaireCombine] = useState({
        "nb-batiments": {},
        "surface-capteur": {},
    })
    const [isGrid, setIsGrid] = useState(false);
    const metaData = {
        "puissance-pv-sol": {
            label: "Puissance crête nouvellement installée (MWc)",
            type: "number",
            default: 0,
            min: 0,
        },
        "production-bois": {
            label: "Production supplémentaire (MWh)",
            type: "number",
            default: 0,
            min: 0,
        },
        "nb-batiments": {
            label: "Nombre de bâtiments collectifs équipés",
            type: "number",
            default: 0,
            min: 0,
        },
        "surface-capteur": {
            label: "Surface de capteurs par bâtiment (m²)",
            type: "number",
            default: 100,
            min: 0,
        },
    };

    return (
        <div className="App">
            <header>
                <img src="header.png" alt="" />
            </header>
            <div className="content">
                <label><input type="checkbox" checked={isGrid} onChange={(e) => setIsGrid(e.target.checked)} /> Version grille</label>
                <h3>Centrale photovoltaïque au sol</h3>
                {isGrid
                    ? <GridTable metaData={metaData} data={dataPV} setData={(val) => setDataPV(val)} />
                    : <ObjectiveTable metaData={metaData} data={dataPV} setData={(val) => setDataPV(val)} />
                }
                <h3>Chaufferie bois</h3>
                {isGrid
                    ? <GridTable metaData={metaData} data={dataBois} setData={(val) => setDataBois(val)} />
                    : <ObjectiveTable metaData={metaData} data={dataBois} setData={(val) => setDataBois(val)} />
                }
                <h3>Installation solaire thermique combinée (chauffage et eau chaude sanitaire)</h3>
                {isGrid
                    ? <GridTable metaData={metaData} data={dataSolaireCombine} setData={(val) => setDataSolaireCombine(val)} />
                    : <ObjectiveTable metaData={metaData} data={dataSolaireCombine} setData={(val) => setDataSolaireCombine(val)} />
                }
            </div>
        </div>
    );
}

function GridTable({ metaData, data, setData }) {
    const years = Array(MAX_YEAR - MIN_YEAR + 1)
        .fill()
        .map((_, index) => MIN_YEAR + index);
    const headerColWidth = Math.max(...Object.keys(data).map((rowId) => metaData[rowId].label.length)) * 10; // 10 ≃ average character size
    const columns = [
        { columnId: "header", width: headerColWidth },
        ...years.map((year) => ({ columnId: `${year}`, width: 60 })),
    ];
    const headRow = { rowId: "years", cells: [
        {type: "header", text: ""},
        ...years.map((year) => ({type: "header", text: ""+year})),
    ]};
    const rows = [
        headRow,
        ...Object.entries(data).map(([rowId, values]) => ({
            rowId,
            cells: [
                {type: "header", text: metaData[rowId].label},
                ...years.map((year) => (
                    values[year] == null
                    ? { type: "number", value: 0, style: { color: "#777" } }
                    : { type: "number", value: values[year] }
                ))
            ],
        }))
    ];
    /*const columns = [{columnId: "a"}, {columnId: "b"}];
    const rows = [
        {rowId:"header", cells: [{type: "header", text: "A"}, {type: "header", text: "B"}]},
        {rowId:"c", cells: [{type: "text", text: "1"}, {type: "text", text: "2"}]},
    ];*/

    const handleChange = (changes) => {
        setData((prevData) => {
            changes.forEach(({ rowId, columnId, newCell }) => {
                if (isNaN(newCell.value)) {
                    delete prevData[rowId][columnId];
                } else {
                    prevData[rowId][columnId] = newCell.value;
                }
            })
            return {...prevData};
        })
    }

    return (
        <div className="grid-container">
            <ReactGrid rows={rows} columns={columns} onCellsChanged={handleChange} enableFillHandle enableRangeSelection />
        </div>
    );
}

function ObjectiveTable({ metaData, data, setData }) {
    const [showPopup, setShowPopup] = useState(false);

    const years = Object.keys(Object.values(data)[0])

    const addObjective = (event) => {
        event.stopPropagation();
        const form = event.target.elements;
        setData((prevData) => {
            Object.entries(prevData).forEach(([rowId, yearValues]) => {
                yearValues[form.year.value] = parseFloat(form[rowId].value);
            })
            return JSON.parse(JSON.stringify(prevData));
        });
        setShowPopup(false);
    }
    const setObjective = (event) => {
        const [rowId, year] = event.target.name.split("#");
        setData((prevData) => {
            prevData[rowId][year] = parseFloat(event.target.value);
            return JSON.parse(JSON.stringify(prevData));
        });
    }
    return (
        <div>
            <button onClick={() => setShowPopup(true)}>+ Ajouter</button>
            <table>
                <thead>
                    <tr>
                        <th>{" "}</th>
                        {years.map((year) => (<th key={year}>{year}</th>))}
                    </tr>
                </thead>
                <tbody>
                    {Object.entries(data).map(([rowId, yearData]) => (
                        <tr key={rowId}>
                            <td style={{backgroundColor: "lightgray"}}>{metaData[rowId].label}</td>
                            {Object.entries(yearData).map(([year, value]) => (
                                <td key={year}>
                                    <input
                                        type="number" style={{width: "100px"}}
                                        name={rowId+"#"+year} value={value ?? ""}
                                        onChange={setObjective}
                                    />
                                </td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
            {showPopup && (
                <div
                    style={{
                        position: "fixed", left: 0, top: 0, width: "100%",
                        height: "100%", background: "#00000020",
                    }}
                    onClick={(e) => { if (e.target === e.currentTarget) setShowPopup(false); }}
                >
                    <div
                        className="popup"
                        style={{
                            margin: "100px auto", width: "fit-content",
                            background: "#fff", padding: "40px",
                        }}
                    >
                        <button onClick={() => setShowPopup(false)}>Annuler</button>
                        <h3>Ajouter un objectif</h3>
                        <form onSubmit={addObjective}>
                            <label>
                                Année{" "}
                                <input type="number" name="year" min={MIN_YEAR} max={MAX_YEAR} required />
                            </label>
                            <br/>
                            {Object.keys(data).map((rowId) => (<>
                                <label>
                                    {metaData[rowId].label}{" "}
                                    <input type="number" name={rowId} />
                                </label>
                                <br/>
                            </>))}
                            <input type="submit" value="Ok" />
                        </form>
                    </div>
                </div>
            )}
        </div>
    );
}

export default App;
